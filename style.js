function createNewUser() {
  let newUser = {
    firstName: "",
    lastName: "",
    birthday: "",

    getLogin() {
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    },

    getAge() {
      let nowDate = Date.now();
      let userBirthday = this.birthday.slice(6) + "-" + this.birthday.slice(3, 5) + "-" + this.birthday.slice(0, 2);
      let userBirthdayDate = Date.parse(userBirthday);
      let userAge = Math.trunc((nowDate - userBirthdayDate) / 1000 / 60 / 60 / 24 / 365.25);
      return userAge;
    },
    
    getPassword() {
        return (this.firstName.toUpperCase()[0] + this.lastName.toLowerCase() + this.birthday.slice(6));
    },

    setLastName(newLastName) {
      return Object.defineProperty(newUser, "firstName", {
        value: newLastName,
      });
    },
  };
  
  Object.defineProperty(newUser, "firstName", {
    value: prompt("Enter first name"),
    writable: false,
    configurable: true,
  });

  Object.defineProperty(newUser, "lastName", {
    value: prompt("Enter last name"),
    writable: false,
    configurable: true,
  });
  
  Object.defineProperty(newUser, "birthday", {
    value: prompt("Enter your birthday", "dd.mm.yyyy"),
  });

  return newUser;
}

const myUser = createNewUser();
const myUserLogin = myUser.getLogin();
const myUserPassword = myUser.getPassword();
const myUserAge = myUser.getAge();

console.log(myUser);
console.log(myUserLogin);
console.log(myUserPassword);
console.log(myUserAge);